<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'HalamanUtama']);
Route::get('/register', [AuthController::class, 'Register']);
Route::post('/welcome', [AuthController::class, 'Welcome']);

Route::get('/table', function () {
    return view(('table'));
});
Route::get('/data-table', function () {
    return view(('datatable'));
});
Route::get('/', function () {
    return view(('home'));
});

//CRUD cast
//create data
Route::get('/cast/create', [CastController::class, 'create']);
//simpan data ke database
Route::post('/cast', [CastController::class, 'store']);
//read data
Route::get('/cast', [CastController::class, 'index']);
//menampilkan data sesuai id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
//Edit Data
Route::get('cast/{cast_id}/edit', [CastController::class, 'edit']);
//update data
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
//delete data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
