@extends('layout.master')
@section('judul')
Home
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-3">tambah</a>
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($data as $key => $item)  
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>
              <form action="/cast/{{$item->id}}" method="POST">

                <a href="/cast/{{$item->id}}" class="btn- btn-info btn-sm">Bio</a>
                <a href="/cast/{{$item->id}}/edit" class="btn- btn-warning btn-sm">Edit Data</a>
                @method('delete')
                @csrf
                <input type="button" value="Delete" class="btn btn-danger btn-sm">
              </form>
            </td>
          </tr>
        @empty
            
        @endforelse
      
    </tbody>
  </table>
@endsection