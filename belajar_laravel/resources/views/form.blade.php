<!DOCTYPE html>
<html>

<head>
    <title>form latihan</title>
</head>

<body>
    <h2>Buat Account Baru!</h2>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        @csrf
        <label>Nama Depan</label><br>
        <input type="text" name="fname"><br><br>
        <label>Nama Belakang</label><br>
        <input type="text" name="lname"><br><br>
        <label>Gender :</label> <br><br>
        <input type="radio">Male <br>
        <input type="radio">Female <br>
        <input type="radio">Other <br><br>
        <label>Nationality</label><br><br>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singapore">Singapore</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="15"></textarea><br>

        <input type="submit" value="kirim">
    </form>
</body>

</html>